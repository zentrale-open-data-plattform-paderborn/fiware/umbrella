# API Umbrella

## General
All traffic is routed via API Proxy, which allows access control. End users shall not access API-umbrella. API Proxy is based on the NREL/Api-umbrella.

API Umbrella is an open source API management platform for exposing web service APIs. The basic goal of API Umbrella is to make life easier for both API creators and API consumers. In this configuration API Proxy is controlled partially by API management.

Technically it is possbile to expose Context Broker or QuantumLeap directly to Internet, but this is not recommeded. Context Broker or QuantumLeap do not offer any restrictions; this scenario would allow anyone to do any operation (Think DELETE / PUT /POST) on those components. It is possible to use another proxy, but we have not worked on this.

Original documentation can be found here: https://api-umbrella.readthedocs.io/en/latest/
## Usage of Umbrella
All external traffic is routed via Umbrella. APIs and website backends can be registered by Admin users in Umbrella. Various controls / [access restrictions](https://api-umbrella.readthedocs.io/en/latest/api-consumer/api-key-usage.html) can be placed on the APIs.
You can learn more from the official documentation for [API backends](https://api-umbrella.readthedocs.io/en/latest/admin/api-backends/index.html) and [Website backends](https://api-umbrella.readthedocs.io/en/latest/admin/website-backends.html)

Umbrella also does traffic logging, so analytics can be stored. APInf platfrom leverages these.  
You can learn more about analytics here: https://api-umbrella.readthedocs.io/en/latest/developer/analytics-architecture.html

## Versioning  
Tested on:  
Version profirator/api-umbrella:0.15.3 (DockerHub digest: sha256:977c777b08eac7f06c1c2a39d47cbfe8a51d79f76aaf5e4ac89f14e7cee03c03)
Version elasticsearch:2.4 (DockerHub digest: sha256:41ed3a1a16b63de740767944d5405843db00e55058626c22838f23b413aa4a39)

## Volume Usage
Umbrella-Elasticsearch uses a persistent directory to store the data (/usr/share/elasticsearch/data)
PVC: NAMESPACE-esdata-umbrella-STATEFULSET-NAME

## Load-Balancing
Currently only one Replica is deployed.

## Route (DNS)
No access to public recommended: Umbrella UI can be accessed via: https://umbrella.fiware.opendata-CITY.de/admin  

## Access the UI
First user to visit https://umbrella.fiware.opendata-CITY.de/admin will be re-directed to sign-up page and first registered user will become the Administrative user of Umbrella.

## Further Testing
The image is taken as is. No further component testing is done.

## Deployment
In order to deploy, following Gitlab-Variables and K8s-secrets are needed:
### Gitlab-Variables
GITLAB_ACCESS_TOKEN - This variable contains the token used to access Gitlab Docker image registry  
KUBECONFIG - This variable contains the content of the Kube.cfg used to access the cluster for dev- and staging-stage.  
KUBECONFIG_PROD - This variable contains the content of the Kube.cfg used to access the cluster for prod-stage.  
NAMESPACE_DEV - This variable contains the namespace for dev-stage used by k8s  
NAMESPACE_STAGING - This variable contains the namespace for staging-stage used by k8s  
NAMESPACE_PROD - This variable contains the namespace for production-stage used by k8s  
UMBRELLA_DEV_DOMAIN - This variable contains the domain name used for the component in dev-stage.  
UMBRELLA_DEV_IP - This variable contains Umbrella's IP in dev-stage.  
UMBRELLA_PROD_DOMAIN - This variable contains the domain name used for the component in production-stage.  
UMBRELLA_PROD_IP - This variable contains Umbrella's IP in production-stage.  
UMBRELLA_STAGING_DOMAIN - This variable contains the domain name used for the component in staging-stage.  
UMBRELLA_STAGING_IP - This variable contains Umbrella's IP in staging-stage.
### Kubernetes Secrets
Create kubernetes-secret for your domain certificates  
`kubectl create secret generic umb-privkey --from-file=/path/to/privkey.pem --namespace=fiware-dev|fiware-staging|fiware-prod`  
`kubectl create secret generic umb-fullchain --from-file=/path/to/fullchain.pem --namespace=fiware-dev|fiware-staging|fiware-prod`  

Create kubernetes-secret for your Maxmind license key  
`kubectl create secret generic maxmind-license --from-literal=license=YOURLICENSE --namespace=fiware-dev|fiware-staging|fiware-prod`  

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE](LICENSE) for additional information.
