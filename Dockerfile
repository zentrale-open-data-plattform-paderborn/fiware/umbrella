# Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh
# This file is covered by the EUPL license.
# You may obtain a copy of the licence at
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

#FROM apinf/api-umbrella:0.15.0-apinf2.1
FROM profirator/api-umbrella:0.15.3

COPY config/api-umbrella.yml /etc/api-umbrella/api-umbrella.yml
RUN cat /etc/api-umbrella/api-umbrella.yml
#COPY config/elasticsearch_setup.lua /app/src/api-umbrella/proxy/jobs/elasticsearch_setup.lua